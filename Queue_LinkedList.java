import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;

public class Queue_LinkedList<Item> implements Iterable<Item> {
    //implements Iterable means, RandomizedQueue<Item> must implement functions from the Iterable interface
    //ex) Iterator<Item> iterator();
    //inner classes can access outer class attributes!, but not using this.

    private Node first, last;
    private int size;

    private class Node {
        Node next;
        Item item;
    }

    // construct an empty randomized queue
    public Queue_LinkedList() {
        this.size = 0;
    }

    // is the randomized queue empty?
    public boolean isEmpty() {
        return this.size == 0;
    }

    // return the number of items on the randomized queue
    public int size() {
        return this.size;
    }

    // add the item
    public void enqueue(Item item) {
        Node oldlast = this.last;
        this.last = new Node();
        this.last.item = item;
//        this.last.next= null;  should be a given?
        if (isEmpty()) {
            this.first = this.last;
            this.size++;
        } else {
            oldlast.next = this.last;
            this.size++;
        }

    }

    // remove and return a random item
    public Item dequeue() {
        Item item = this.first.item;
        this.size--;
        if (isEmpty()) {
            this.first = null;

        } else {
            this.first = this.first.next;

        }
        return item;

    }

    // return a random item (but do not remove it)
    public Item sample() {
        int count = 1;
        int random = StdRandom.uniform(1, this.size + 1);
        while (count != random) {
            this.first = this.first.next;
            count++;
        }
        Item item = this.first.item;
        return item;
    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new ListIterator<Item>();
    }

    private class ListIterator<Item> implements Iterator<Item> {
        //means ListIterator<Item> must have implementations of the methods for Iterator<Item> interface
        //ex) boolean hasNext();
        private Node current = first;

        public boolean hasNext() {

            return !isEmpty();
        }

        public Item next() {
            int count = 1;
            int random = StdRandom.uniform(1, size() + 1);
            while (count != random) {
                this.current = this.current.next;
                count++;
            }

            Item item = (Item) this.current.item;
            this.current = this.current.next;
            return item;
        }

    }

    // unit testing (required)
    public static void main(String[] args) {
        Queue_LinkedList<Integer> chichi = new Queue_LinkedList<Integer>();
        chichi.enqueue(5);
        chichi.enqueue(4);
        chichi.dequeue();
        chichi.enqueue(6);
        chichi.enqueue(7);
        chichi.enqueue(8);
        System.out.println(chichi.sample());


    }

}
