import edu.princeton.cs.algs4.StdIn;

import java.util.Iterator;

public class Permutation {
    public static void main(String[] args) {
        int k = Integer.parseInt(args[0]);
        RandomizedQueue<String> chichi = new RandomizedQueue<String>();

//        final String inputFile = StdIn.readString();
//        In in = new In(inputFile);
//
        while (!StdIn.isEmpty()) {
            String s = StdIn.readString();
            chichi.enqueue(s);
        }
        int count = 1;
        Iterator<String> i = chichi.iterator();

        while (i.hasNext() && count <= k) {
            System.out.println(i.next());
            count++;
        }
    }
}
