import java.util.Iterator;

public class Deque<Item> implements Iterable<Item> {

    private Node first, last;
    private int size;

    private class Node {
        Node next, prev;
        Item item;
    }

    // construct an empty deque
    public Deque() {
        this.size = 0;
        //first, last are all null at instantiation
    }

    // is the deque empty?
    public boolean isEmpty() {
        return this.size == 0;
    }

    // return the number of items on the deque
    public int size() {
        return this.size;
    }

    // add the item to the front
    public void addFirst(Item item) {
        if (item == null)
            throw new IllegalArgumentException();
        Node oldfirst = this.first;
        this.first = new Node();
        this.first.item = item;
        if (isEmpty()) {
            this.last = this.first;
            this.size++;
        } else {
            this.first.next = oldfirst;
            oldfirst.prev = this.first;
            this.size++;
        }
    }

    // add the item to the back
    public void addLast(Item item) {
        if (item == null)
            throw new IllegalArgumentException();
        Node oldlast = this.last;
        this.last = new Node();
        this.last.item = item;
        if (isEmpty()) {
            this.first = this.last;
            this.size++;
        } else {
            oldlast.next = this.last;
            this.last.prev = oldlast;
            this.size++;
        }

    }

    // remove and return the item from the front
    public Item removeFirst() {
        if (this.size == 0)
            throw new java.util.NoSuchElementException();

        Item item = this.first.item;
        this.size--;
        if (isEmpty()) {
            this.first = null;
        } else {
            this.first = this.first.next; //the original will be disposed of by garbage collector
            this.first.prev = null;
        }
        return item;
    }

    // remove and return the item from the back
    public Item removeLast() {
        if (this.size == 0)
            throw new java.util.NoSuchElementException();
        Item item = this.last.item;
        this.size--;
        if (isEmpty()) {
            this.last = null;
        } else {
            this.last = this.last.prev;
            this.last.next = null;
        }
        return item;
    }

    // return an iterator over items in order from front to back
    public Iterator<Item> iterator() {
        return new ListIterator<Item>();
    }

    private class ListIterator<Item> implements Iterator<Item> {
        private Node current;

        public ListIterator() {
            this.current = first;
        }

        public boolean hasNext() {
            return this.current != null; //checking at current iteration..
        }

        public Item next() {
            if (this.current == null)
                throw new java.util.NoSuchElementException();
            Item item = (Item) this.current.item;
            this.current = this.current.next;
            return item;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    // unit testing (required)
    public static void main(String[] args) {
        Deque<Integer> deque = new Deque<Integer>();
        deque.addLast(1);
        deque.addFirst(2);
        deque.addFirst(2);

        deque.removeLast();
        deque.removeFirst();
        System.out.println(deque.isEmpty());
        System.out.println(deque.size());
    }

}
