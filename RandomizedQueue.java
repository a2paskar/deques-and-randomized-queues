import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;

public class RandomizedQueue<Item> implements Iterable<Item> {

    private Item[] s;
    private int head;
    private int tail;

    // construct an empty randomized queue
    public RandomizedQueue() {
        this.s = (Item[]) new Object[1];
        this.head = 0;
        this.tail = 0;
    }

    // is the randomized queue empty?
    public boolean isEmpty() {
        return size() == 0;
    }

    // return the number of items on the randomized queue
    public int size() {
        return this.tail - this.head;
    }

    // add the item
    public void enqueue(Item item) {
        if (item == null)
            throw new IllegalArgumentException();
        this.s[this.tail++] = item; //access head then increment head
        if (size() == this.s.length)
            resize(this.s.length * 2);

    }

    private void resize(int cap) {
        Item[] copy = (Item[]) new Object[cap];
        for (int i = 0; i < this.tail - this.head; i++) {
            copy[i] = this.s[this.head + i];
        }
        this.s = copy;
    }

    private void shuffle() {
        for (int i = this.head; i < this.tail; i++) {
            int r = StdRandom.uniform(this.head, i + 1);
            exchange(i, r);
        }

    }

    private void exchange(int i, int r) {
        Item swap = this.s[i];
        this.s[i] = this.s[r];
        this.s[r] = swap;

    }

    // remove and return a random item
    public Item dequeue() {
        if (size() == 0)
            throw new java.util.NoSuchElementException();
        shuffle();
        //StdRandom.shuffle(this.s, this.head, this.tail);
        Item item = this.s[this.head++];
        this.s[this.head - 1] = null;
        if (size() == this.s.length / 4) {
            resize(this.s.length / 2);
            this.tail = this.tail - this.head;
            this.head = 0;
        }
        return item;

    }

    // return a random item (but do not remove it)
    public Item sample() {
        if (size() == 0)
            throw new java.util.NoSuchElementException();
        int random = StdRandom.uniform(this.head, this.tail);
        return this.s[random];
    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new ListIterator<Item>();
    }

    private class ListIterator<Item> implements Iterator<Item> {
        //means ListIterator<Item> must have implementations of the methods for Iterator<Item> interface
        //ex) boolean hasNext();
        private int i;

        public ListIterator() {
            shuffle();
            //StdRandom.shuffle(s, head, tail);
            this.i = head;
        }

        public boolean hasNext() {
            return this.i != tail; //checking at current iteration..
        }

        public Item next() {
            if (!hasNext())
                throw new java.util.NoSuchElementException();
            Item item = (Item) s[i];
            this.i++;
            return item;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

    }

    // unit testing (required)
    public static void main(String[] args) {
        int n = 5;
        RandomizedQueue<Integer> queue = new RandomizedQueue<Integer>();
        for (int i = 0; i < n; i++)
            queue.enqueue(i);

        queue.dequeue();
        queue.size();
        for (int a : queue) {
            for (int b : queue)
                StdOut.print(a + "-" + b + " ");
            StdOut.println();
        }
    }
}
